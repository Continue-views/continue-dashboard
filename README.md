<div align="center"><img width="200" src="./assets/logo.jpg"/>
<h1> continue-dashboard </h1>
</div>

## 地址

- [Vue2.X](https://gitee.com/Continue-views/continue-dashboard/tree/Vue2.X/)
- [Vue3.X](https://gitee.com/Continue-views/continue-dashboard/tree/Vue3.X/)
- [React16.X](https://gitee.com/Continue-views/continue-dashboard/tree/React16.X/)
- [MiniProgram](https://gitee.com/Continue-views/continue-dashboard/tree/MiniProgram/)

##  Vue2.X [点击切换分支](https://gitee.com/Continue-views/continue-dashboard/tree/Vue2.X/)

```bash
# 克隆项目
git clone -b Vue2.X git@gitee.com:Continue-views/continue-dashboard.git

# 进入项目目录
cd continue-dashboard

# 安装依赖
npm i

# 本地开发 启动项目
npm run serve
```

##  Vue3.X [点击切换分支](https://gitee.com/Continue-views/continue-dashboard/tree/Vue3.X/)

```bash
# 克隆项目
git clone -b Vue3.X git@gitee.com:Continue-views/continue-dashboard.git

# 进入项目目录
cd continue-dashboard
# 安装依赖
npm i
# 本地开发 启动项目
npm run serve
```

##  React16.X [点击切换分支](https://gitee.com/Continue-views/continue-dashboard/tree/React16.X/)

```bash
# 克隆项目
git clone -b React16.X git@gitee.com:Continue-views/continue-dashboard.git

# 进入项目目录
cd continue-dashboard
# 安装依赖
npm i
# 本地开发 启动项目
npm run serve
```

##  MiniProgram [点击切换分支](https://gitee.com/Continue-views/continue-dashboard/tree/MiniProgram/)

```bash
# 克隆项目
git clone -b MiniProgram git@gitee.com:Continue-views/continue-dashboard.git

# 进入项目目录
cd continue-dashboard
# 安装依赖
npm i
# 本地开发 启动项目
npm run serve
```
